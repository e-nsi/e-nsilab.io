# Exercices pratiques de NSI

## Présentation :woman_technologist:

Ce site propose des exercices d'entrainement à la pratique de la programmation en lien avec la spécialité NSI. Le langage utilisé est [Python](https://www.python.org/).

Les exercices proposés sont discutés, testés et rédigés par un collectif de professeurs d’informatique, de lycée ou du supérieur.

Tous les exercices ont subi de nombreux tests avant publication.

## Fonctionnement :key:

Les exercices sont présentés dans trois catégories :

* [Pour démarrer](https://e-nsi.gitlab.io/pratique/N0/) : des exercices pour démarrer facilement. Ils sont guidés et sont applications directes du cours.

* [Exercices à maitriser](https://e-nsi.gitlab.io/pratique/N1/) : des exercices dont les algorithmes sont faciles à concevoir. Idéal pour s'entraîner sur les méthodes classiques (rechercher un maximum, parcourir une chaîne de caractères...).

* [Exercices guidés](https://e-nsi.gitlab.io/pratique/N2/) : des exercices dont les algorithmes sont plus élaborés. Le "squelette" du code est fourni. Il s'agit donc de comprendre le fonctionnement de l'algorithme et de compléter le code.
  
* [Exercices difficiles](https://e-nsi.gitlab.io/pratique/N3/) : des exercices... difficiles ! Il est nécessaire de prendre un papier et un crayon et de faire des tests à la main, de bien comprendre le problème, le formaliser... avant de commencer à saisir du code.

Dans chaque page, on trouve une description du sujet, des exemples d'application et une zone de saisie du code.

Sous cette dernière zone vous trouverez quatre boutons :

* ![Exécuter](images/buttons/icons8-play-64.png) : exécute le code. Cela permet de visualiser les éventuelles erreurs et d'effectuer les tests fournis dans la zone de saisie.
  
* ![Télécharger](images/buttons/icons8-download-64.png) : télécharger le code. Pour travailler dans votre IDE préféré ! 
  
* ![Téléverser](images/buttons/icons8-upload-64.png) : téléverser un code depuis votre appareil. Votre code fonctionne en local... mais passe-t'il les tests secrets ?
  
* ![Tester](images/buttons/icons8-check-64.png) : exécute le code et aussi **des tests secrets**. C'est la vérification finale : réussir cette étape indique que l'on a réussi l'exercice !

## Quels exercices traiter ? :compass:

Vous pouvez choisir les exercices de plusieurs façons :

* en choisissant le niveau de difficulté (exercices [faciles](https://e-nsi.gitlab.io/pratique/N1/anniversaires/sujet/), [guidés](https://e-nsi.gitlab.io/pratique/N2/), [difficiles](https://e-nsi.gitlab.io/pratique/N3/))

* en choisissant de travailler un thème particulier. Les exercices sont *taggés* selon le thème abordé (les boucles, les chaînes de caractères, *etc*...). Vous trouverez la liste des tags sur [cette page](https://e-nsi.gitlab.io/pratique/tags/)

## Comment est construit ce site ? :hammer_and_wrench:

Ce site est rédigé en [Markdown](https://markdown.org), construit de façon statique avec [MkDocs](https://mkdocs.org) et le thème [Material](https://squidfunk.github.io/mkdocs-material/). 

Les logiciels qui servent à construire et publier ce site sont des [logiciels libres](https://www.april.org/articles/divers/intro_ll.html) et respectent les 4 libertés :

1. Utiliser le logiciel pour tous les usages
2. Étudier le code source du logiciel
3. Distribuer le logiciel de façon libre
4. Améliorer le logiciel et distribuer les améliorations.

Le site fonctionne donc sans tracker de suivi ni cookies (aucune mesure d'audience). Le code JavaScript exécuté sert pour la console de développement intégré sur les pages et nous l'hébergeons également. Seul [MathJax](https://www.mathjax.org/) (utilisé pour le rendu des formules mathématiques) est servi depuis un réseau de diffusion de contenu (ou *CDN*, [jsdeliver.net](https://www.jsdelivr.com/) en l'occurrence).

Les auteurs des exercices sont cités dans le code source `html` de la page.
